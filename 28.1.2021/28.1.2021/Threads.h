#pragma once
#include <iostream>
#include <thread>
#include <vector>
#include <fstream>
#include <time.h>

void I_Love_Threads();
void call_I_Love_Threads();
bool isPrime(int n);
void getPrimes(int begin, int end, std::vector<int>& primes);
void printVector(std::vector<int> primes);
std::vector<int> callGetPrimes(int begin, int end);
void writePrimesToFile(int begin, int end, std::ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);

