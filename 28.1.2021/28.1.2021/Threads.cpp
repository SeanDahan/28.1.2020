#include "Threads.h"

void I_Love_Threads()
{
	std::cout << "I love threads" << std::endl;
}


void call_I_Love_Threads()
{
	std::thread thr(I_Love_Threads);
	thr.join();
}


bool isPrime(int n) 
{
	int i = 0;
	for (i = 2; i <= n / 2; ++i) 
	{
		if (n % i == 0) 
		{
			return false;
		}
	}
	return true;
}


void getPrimes(int begin, int end, std::vector<int>& primes)
{
	for (size_t i = begin; i < end; i++)
	{
		if (isPrime(i))
		{
			primes.push_back(i);
		}
	}
}


void printVector(std::vector<int> primes)
{
	for (auto val : primes)
	{
		std::cout << val << std::endl;
	}
}


std::vector<int> callGetPrimes(int begin, int end)
{
	std::vector<int> vec;
	std::thread thr(getPrimes, begin, end, std::ref(vec));
	thr.join();
	std::thread thr2(printVector, vec);
	thr2.join();

	return vec;
}


void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	bool flag = true;
	for (size_t j = begin; j < end; j++)
	{
		flag = true;
		for (size_t i = 2; i <= j / 2; ++i)
		{
			if (j % i == 0)
			{
				flag =  false;
			}
		}
		if (flag)
		{
			file << j << "\n";
		}
	}
	
}


void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream file;
	file.open(filePath, std::ios::out);

	clock_t start, endTime;
	start = clock();
	int tempEnd = begin + end / N;
	for (size_t i = begin; i < end; i+=end/N)
	{
		
		tempEnd = i + end / N;
		std::thread thr(writePrimesToFile,i , tempEnd, std::ref(file));
		thr.join();
		
	}
	endTime = clock();
	std::cout << "Total time taken " << ((double)(endTime - start)) / CLOCKS_PER_SEC << " Sec" << std::endl;
	
}
